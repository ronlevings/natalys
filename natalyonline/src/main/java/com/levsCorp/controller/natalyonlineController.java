package com.levsCorp.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class natalyonlineController {
	
	@Value("${spring.application.name}")
    String appName;
 
    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        return "home";
    }
    

    @GetMapping("/error")
    public String errPage(Model model) {
        model.addAttribute("appName", appName);
        return "home";
    }

}
