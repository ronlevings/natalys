package com.levsCorp.natalyonline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.levsCorp.controller.natalyonlineController;

@SpringBootApplication
@ComponentScan(basePackageClasses=natalyonlineController.class)
public class NatalyonlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(NatalyonlineApplication.class, args);
	}

}
